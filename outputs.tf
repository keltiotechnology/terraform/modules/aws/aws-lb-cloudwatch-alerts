output "alarm_unhealthy_service_arn" {
  description = "A list of ARN of the cloudwatch metric alarms to alert when some service are not healthy on load balancer target groups"
  value       = aws_cloudwatch_metric_alarm.unhealthy_service.*.arn
}

output "alarm_error_http_routes_arn" {
  description = "A list of ARN of the cloudwatch metric alarms to alert when external http request does not send a 2xx on multiple routes"
  value       = aws_cloudwatch_metric_alarm.error_http_routes.*.arn
}
